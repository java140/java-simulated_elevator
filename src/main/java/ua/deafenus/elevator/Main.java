package ua.deafenus.elevator;

import static ua.deafenus.elevator.Application.*;

public class Main {
    public static void main(String[] args) {
        Application application = new Application();
        Application.run();
    }
}
